﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class wwiseFootsteps : MonoBehaviour
{

    public AK.Wwise.Event footstepsevent; //wwise event
    private vThirdPersonInput tpInput; //обращаемся к магнитуде
    public LayerMask layer;
    string typeSurface;

    public GameObject leftfoot;
    public GameObject rightfoot;
    
    
    public AK.Wwise.Event footstepsrunevent; //event на бег
    
    
    
    // Start is called before the first frame update
    void Start()
    {
       
        tpInput = gameObject.GetComponent<vThirdPersonInput>(); // 
        
    }
    


    // void footstep()
    // {
    //     if (tpInput.cc.inputMagnitude > 0.1)
    //     {
    //         MaterialCheck();
    //         AkSoundEngine.SetSwitch("SWITCH_surface_type", typeSurface, gameObject);
    //         footstepsevent.Post(gameObject);
    //         
    //     }
    //
    // }
    
    
    
    void footstep(string leg)
    {
        if (tpInput.cc.inputMagnitude > 0.1) //проверка магнитуды
        {
            //Debug.Log(leg);
            if (leg == "right") // если на обьекте правая нога то...
            {
                PlaySteps(rightfoot); //воспроизводим правый звук шагов
            }
            else if (leg == "left")
            {
                PlaySteps(leftfoot);
            }
        }

    }


    void PlaySteps(GameObject legObject) //обьект от ног права и лево мы передаем сюда в переменную legObject
    {
        if (Physics.Raycast(legObject.transform.position, Vector3.down, out RaycastHit rayHit, 0.3f, layer))
        {
            //Debug.Log(rayHit.collider.tag);
            AkSoundEngine.SetSwitch("SWITCH_surface_type", rayHit.collider.tag, legObject);
            footstepsevent.Post(legObject); //запуск ивенка на обьекте

        }
    }
    
    
    
    void run(string leg)
    {
        if (tpInput.cc.inputMagnitude > 1.3) //проверка магнитуды
        {
            //Debug.Log(leg);
            if (leg == "right") // если на обьекте правая нога то...
            {
                PlayStepsRun(rightfoot); //воспроизводим правый звук шагов
            }
            else if (leg == "left")
            {
                PlayStepsRun(leftfoot);
            }
        }

    }
    
    
    void PlayStepsRun(GameObject legRunObject) //обьект от ног права и лево мы передаем сюда в переменную legObject
    {
        if (Physics.Raycast(legRunObject.transform.position, Vector3.down, out RaycastHit rayHit, 0.3f, layer))
        {
            //Debug.Log(rayHit.collider.tag);
            AkSoundEngine.SetSwitch("SWITCH_surface_type", rayHit.collider.tag, legRunObject);
            footstepsrunevent.Post(legRunObject); //запуск ивенка на обьекте

        }
    }
    
    
    
    // Update is called once per frame
    void Update()
    {
        
    }
    
    
    
}
