﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Event = AK.Wwise.Event;

public class simpleocclusion : MonoBehaviour
{
    public AK.Wwise.Event wwevent;
    public GameObject camera;
    public LayerMask lm;
    
    // Start is called before the first frame update
    void Start()
    {
        wwevent.Post(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Physics.Linecast(gameObject.transform.position, camera.transform.position, out hit, lm);
        if (hit.collider)
        {
            Debug.DrawLine(gameObject.transform.position, camera.transform.position, Color.red);
            AkSoundEngine.SetRTPCValue("RTPC_Occlusion", 1);
        }
        else
        {
            Debug.DrawLine(gameObject.transform.position, camera.transform.position, Color.green);
            AkSoundEngine.SetRTPCValue("RTPC_Occlusion", 0);
        }
    }
}
