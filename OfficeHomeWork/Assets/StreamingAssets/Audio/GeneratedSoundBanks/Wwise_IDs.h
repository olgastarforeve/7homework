/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AQUARIUM = 456876952U;
        static const AkUniqueID ARCADE = 1946377065U;
        static const AkUniqueID FAN = 1183803264U;
        static const AkUniqueID FLIES = 1457611216U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID OUTDOOR = 144697359U;
        static const AkUniqueID RADIO = 2548238350U;
        static const AkUniqueID ROOM_TONE = 1272037051U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace STATE_INDOOR
        {
            static const AkUniqueID GROUP = 3825359022U;

            namespace STATE
            {
                static const AkUniqueID INDOOR = 340398852U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OUTDOR = 1069479858U;
            } // namespace STATE
        } // namespace STATE_INDOOR

    } // namespace STATES

    namespace SWITCHES
    {
        namespace SWITCH_SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 2818941756U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_SURFACE_CARPET = 117396797U;
                static const AkUniqueID SWITCH_SURFACE_CONCRETE = 3294452117U;
                static const AkUniqueID SWITCH_SURFACE_TILE = 2754391064U;
                static const AkUniqueID SWITCH_SURFACE_WOOD = 156175339U;
            } // namespace SWITCH
        } // namespace SWITCH_SURFACE_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID CAMERA_HEIGHT = 587070286U;
        static const AkUniqueID RTPC_OCCLUSION = 1664992182U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMB_INDOOR = 3997975935U;
        static const AkUniqueID AMB_OUTSIDE = 1013211851U;
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID REVERB1 = 3545700926U;
        static const AkUniqueID REVERB2 = 3545700925U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
